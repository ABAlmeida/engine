#include "Object.h"
#include "factory\TypeInfo.h"

Object::Object()
{

}

Object::~Object()
{

}

const char* Object::kType = "Object";

int Object::type_id() const
{
	return m_info->type_id;
}

const char* const Object::type_name()
{
	return m_info->type_name.c_str();
}

bool Object::is(int type) const
{
	return type == kTypeId;
}

template<typename T>
T* Object::as()
{
	return is(T::kTypeId) ? (T*)this : 0;
}

template<typename T>
const T* Object::as() const
{
	return is(T::kTypeId) ? (const T*)this : 0;
}