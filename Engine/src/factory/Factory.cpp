#include "factory\Factory.h"
#include "Object.h"

Factory::Factory()
{

}

Factory::~Factory()
{

}

void Factory::registerTypeInfo(int id, const char* name, Creator create)
{
	TypeInfo* info = new TypeInfo();
	info->type_id = id;
	info->type_name = name;
	info->create = create;
	m_types.push_back(info);
}

void Factory::unregisterTypeInfo(int id)
{
	size_t size = m_types.size();
	for (size_t i = 0; i < size; ++i)
	{
		if (m_types[i]->type_id == id)
		{
			delete m_types[i];
			m_types.erase(m_types.begin() + i);
			break;
		}
	}
}

Object* Factory::create(int id)
{
	size_t size = m_types.size();
	for (size_t i = 0; i < size; ++i)
	{
		if (m_types[i]->type_id == id)
		{
			return m_types[i]->create();
		}
	}
	return 0;
}

Object* Factory::create(const char* type)
{
	size_t size = m_types.size();
	for (size_t i = 0; i < size; ++i)
	{
		if (m_types[i]->type_name == type)
		{
			return m_types[i]->create();
		}
	}
	return 0;
}