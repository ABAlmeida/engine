#pragma once
#include <iostream>
#include "Application.h"

int main(int argc, char* argv[])
{
	const int width = 800;
	const int height = 600;

	Application application;
	
	application.onInitialise(width, height);

	int play = 1;

	while (play != 0)
	{
		play = application.onInput();

		application.onUpdate();

		application.onDraw();

		application.onGUI();
	}

	SDL_Quit();

	return 0;
}