#include "Application.h"

Application::Application()
{

}

Application::~Application()
{
	m_scenes->unregisterView(m_views[0]);

	delete m_scenes;

	size_t size = m_views.size();

	for (size_t i = 0; i < size; i++)
	{
		delete m_views[0];
		m_views.erase(m_views.begin());
	}

	delete m_graphics;
	delete m_factory;
}

void Application::onInitialise(const int w, const int h)
{
	// Initialise all member pointers
	m_graphics = new Graphics(w, h);

	m_factory = new Factory();

	m_scenes = new Scene(m_factory);

	m_views.push_back(new Renderer());

	m_scenes->registerView(m_views[0]);

	m_inputReader = new InputReader();

	m_dt = 0;
	m_lastdt = 0;
}

int Application::onInput()
{
	return m_inputReader->readInput();
}

void Application::onUpdate()
{
	// Retrieve dt
	m_dt = deltaTime();
}

void Application::onDraw()
{
	// 3D Drawing Set Up, Must be called first
	m_graphics->Render3D();
}

void Application::onGUI()
{
	// UI Drawing Set Up, must be called first
	m_graphics->RenderUI();

	// Render everything to our screen, Must be called after all draw calls
	m_graphics->Render();
}