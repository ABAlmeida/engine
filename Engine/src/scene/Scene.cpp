#include "scene\Scene.h"
#include "Object.h"
#include "view\Observer.h"

Scene::Scene(Factory* factory)
{
	m_factory = factory;
}

Scene::~Scene()
{
	size_t size = m_objects.size();

	for (size_t i = 0; i < size; ++i)
	{
		destroy(m_objects[0]);
	}

	m_factory = nullptr;
}

Object* Scene::create(int type)
{
	Object* temp = m_factory->create(type);
	if (temp)
	{
		m_objects.push_back(temp);
	}
	return temp;
}

void Scene::destroy(Object* object)
{
	size_t size = m_objects.size();
	for (size_t i = 0; i < size; ++i)
	{
		if (m_objects[i] == object)
		{
			delete m_objects[i];
			m_objects.erase(m_objects.begin() + i);
			break;
		}
	}
}

size_t Scene::numObjects() const
{
	return m_objects.size();
}

Object* Scene::getObject(int index)
{
	return m_objects[index];
}

void Scene::registerView(Observer* view)
{
	m_views.push_back(view);
}

void Scene::unregisterView(Observer* view)
{
	size_t size = m_views.size();
	for (size_t i = 0; i < size; i++)
	{
		if (m_views[i] == view)
		{
			m_views.erase(m_views.begin() + i);
			return;
		}
	}
}