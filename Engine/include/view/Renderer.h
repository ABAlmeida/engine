#pragma once
#include "view\Observer.h"
#include "GL\glew.h"
#include <vector>

class Renderer : public Observer
{
public:
	void onCreate(Object* obj);
	void onModified(Object* obj);
	void onDestroy(Object* obj);
private:
	std::vector<GLuint> m_vbo;
	std::vector<GLuint> m_ibo;
	std::vector<GLuint> m_vao;
};