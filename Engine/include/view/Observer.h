#pragma once

class Object;

class Observer
{
public:
	virtual void onCreate(Object* obj) = 0;
	virtual void onModified(Object* obj) = 0;
	virtual void onDestroy(Object* obj) = 0;
};