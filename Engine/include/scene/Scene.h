//----------------------------------------------------------------------------------------------------------------------
/// \file Scene.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The base for all scenes
//----------------------------------------------------------------------------------------------------------------------
#pragma once
#include "factory\Factory.h"

class Object;
class Observer;


class Scene
{
public:
	Scene(Factory* factory);
	~Scene();

	Object* create(int type);
	void destroy(Object* object);
	size_t numObjects() const;
	Object* getObject(int index);
	void registerView(Observer* view);
	void unregisterView(Observer* view);
private:
	std::vector<Object*> m_objects;
	std::vector<Observer*> m_views;
	Factory* m_factory;
};