//----------------------------------------------------------------------------------------------------------------------
/// \file Graphics.h
/// \author Antonio Almeida
/// \date 10/03/2016
/// \brief Initalizes OpenGL and SDL graphics for the engine.
//----------------------------------------------------------------------------------------------------------------------
#pragma once
#include "gl/glew.h"
#include "SDL/SDL.h"
#include <iostream>

class Graphics
{
public:
	Graphics(const int width, const int height);
	~Graphics();

	void Render();
	void Render3D();
	void RenderUI();
private:
	void initializeSDL();
	void createWindow(const int width, const int height);
	void initializeGLEW();

	SDL_Window *m_window;
	SDL_GLContext m_context;
};