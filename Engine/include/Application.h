//----------------------------------------------------------------------------------------------------------------------
/// \file Application.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main application class for the engine.
//----------------------------------------------------------------------------------------------------------------------
#pragma once
#include <memory>
#include "view\Renderer.h"
#include "graphics\Graphics.h"
#include "input\InputReader.h"
#include "scene\Scene.h"

class Application
{
public:
	Application();
	~Application();

	void onInitialise(const int w, const int h);
	int onInput();
	void onUpdate();
	void onDraw();
	void onGUI();
private:
	inline float deltaTime()
	{
		unsigned int current = SDL_GetTicks();
		// Next, we want to work out the change in time between the previous frame and the current one
		// This is a 'delta' (used in physics to denote a change in something)
		// So we call it the 'deltaT' and I like to use an 's' to remind me that it's in seconds!
		// (To get it in seconds we need to divide by 1000 to convert from milliseconds)
		float deltaTs = static_cast<float>(current - m_lastdt) * 0.001f;
		m_lastdt = current;
		return deltaTs;
	}

	Graphics* m_graphics;
	InputReader* m_inputReader;
	Scene* m_scenes;
	std::vector<Renderer*> m_views;
	Factory* m_factory;

	float m_dt;
	unsigned int m_lastdt;
};