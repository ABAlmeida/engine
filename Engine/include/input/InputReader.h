//----------------------------------------------------------------------------------------------------------------------
/// \file InputReader.h
/// \author Antonio Almeida
/// \date 06/06/2015
/// \brief The main input class for the engine.
//----------------------------------------------------------------------------------------------------------------------
#pragma once
#include "SDL\SDL.h"

enum inputValue
{
	//Exit
	kEsc = 0,

	kLArrow, kRArrow, kUArrow, kDArrow,
	kSpace, kEnter,

	kP,

	//Mouse Controls
	kLMouse, kRMouse, kMMouse,
};

class InputReader
{
public:
	InputReader();
	~InputReader();

	int readInput();
};