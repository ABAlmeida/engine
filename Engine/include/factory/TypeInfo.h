#pragma once
#include <string>

class Object;

typedef Object* (*Creator)();

class TypeInfo
{
public:
	TypeInfo();
	~TypeInfo();

	int type_id;
	std::string type_name;
	Creator create;
private:
	
};