#pragma once
#include <vector>
#include "TypeInfo.h"

class TypeInfo;
class Object;

class Factory
{
public:
	Factory();
	~Factory();

	void registerTypeInfo(int id, const char* name, Creator create);
	void unregisterTypeInfo(int id);

	Object* create(const int type);
	Object* create(const char* type);
private:
	std::vector<TypeInfo*> m_types;
};