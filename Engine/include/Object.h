#pragma once
#include "factory\Factory.h"

class TypeInfo;

class Object
{
public:
	Object();
	~Object();

	friend class Factory;

	static const char* kType;
	static const int kTypeId = 0;

	int type_id() const;
	const char* const type_name();

	virtual bool is(int type) const;
	template<typename T>
	T* as();
	template<typename T>
	const T* as() const;
private:
	TypeInfo* m_info;
};